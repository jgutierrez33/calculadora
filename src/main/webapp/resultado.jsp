<%-- 
    Document   : resultado
    Created on : 11-04-2021, 20:44:22
    Author     : JGM
--%>

<%@page import="modelo.calculador"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
  calculador cal=(calculador)request.getAttribute("calculador");
 
%>
 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1><%= cal.getResultado()%></h1>
        
        <form name="form" action="CalculadoraControler" method="POST">  
     <div class="form-group">
                        <label for="codigo">Resultado de la suma <%= cal.getResultado()%></label>
                        <input  name="codigo" value="<%= cal.getResultado()%>" class="form-control" required id="codigo" aria-describedby="usernameHelp">
                        </div>
    </body>
    
  </html>
